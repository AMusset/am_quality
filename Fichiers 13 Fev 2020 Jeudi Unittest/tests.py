import unittest
from spliter import my_lister

file = open(r'C:\Users\Alexis Musset\PycharmProjects\am_quality\ex_quality\list.txt', 'r')
# list1 = file.read()
file.close()

list1 = "465)-19)-6574)-65486)-5468)89)321)684)-55"


class TestMyLister(unittest.TestCase):

    def test_list_has_right_length(self):
        a = "6 5 8 7"
        b = ["6", "5", "8", "7"]
        self.assertEqual(len(my_lister(a, " ")), len(b))

    def test_list_not_int(self):
        a = "6 5 8 7"
        b = [6, 5, 8, 7]
        self.assertNotEqual(my_lister(a, " "), b)

    def test_is_list(self):
        self.assertEqual(type(my_lister(list1, " ")), type([]))

    # def test_is_all_types(self):
    #     tablSeparateur = [" ", None, "+", 0, "@||"]
    #     for separateur in tablSeparateur:
    #         self.assertEqual(my_lister(list1, separateur), [])

    def test_with_different_separator(self):
        separateur = ")"
        a = "465)-19)-6574)-65486)-5468)89)321)684)-55"
        b = ["465", "-19", "-6574", "-65486", "-5468", "89", "321", "684", "-55"]
        self.assertEqual(my_lister(a, separateur), b)

    # def test_is_list1(self):
    #     self.assertEqual(my_lister(), []).addTypeEqualityFunc(self)


unittest.main()

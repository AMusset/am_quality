# Il doit y avoir un dictionnaire en retour nommé bossDict
# Ce bossDict contient 2 éléments
# Les keys de ce dictionnaire sont respectivement "chameau" et "dromadaire"
# Les éléments respectifs associés à ces clefs sont des int et sont supérieur à 0
# Le résultat "res" doit être égal au nombre de chameau * 2 + le nombre de dromadaire * 1 (bosses)


# Conclusion : dans les tests,
# theBossFunction est appelé sans arguments dans le setUp
# theBossFunction est appelé avec un argument ( self.bossDict ) dans le test_calculDeBosses

boss_dico = {"chameau": 4, "dromadaire": 5}


def theBossFunction(b_dico):
    res = b_dico["chameau"]*2 + b_dico["dromadaire"]*1
    return b_dico

import operator
from pip._vendor.distlib.compat import raw_input

# Saisie du premier nombre - Répétition tant que ce qui est saisi n'est pas un nombre
while True:
    try:
        nbr1 = int(raw_input("Saisissez un premier nombre : "))
        break
    except ValueError:
        print("Ceci n'est pas un nombre.")

# Traduction des opérateurs
operateurs = {"+": operator.add,
              "-": operator.sub,
              "*": operator.mul,
              "/": operator.truediv,
              "^": operator.xor,
              "**": operator.pow,
              "%": operator.mod}

# Saisie de l'opérateur : L'opérateur doit correspondre à un des symboles indiqués
operateur = "0"
while operateur not in operateurs:
    operateur = input("Saisissez un opérateur (+, -, *, /, ^, %, **) : ")

# Saisie du second nombre - Répétition tant que ce qui est saisi n'est pas un nombre
while True:
    try:
        nbr2 = int(raw_input("Saisissez un second nombre : "))
        break
    except ValueError:
        print("Ceci n'est pas un nombre.")

# Calcul du résultat, affichage et saisie du fichier txt de destination
resultat = operateurs[operateur](int(nbr1), int(nbr2))
print("Résultat : ", nbr1, operateur, nbr2, "=", resultat)
f = input("Dans quel fichier souhaitez-vous enregistrer l'opération ? ")

# Ecriture ("Append") dans le fichier
fichier = open(f, 'a')
fichier.write('{0} {1} {2} = {3}\n'.format(nbr1, operateur, nbr2, resultat))
fichier.close()

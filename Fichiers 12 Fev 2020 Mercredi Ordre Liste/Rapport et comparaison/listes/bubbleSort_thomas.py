#!/usr/bin/env python3
import sys
import random
from datetime import datetime
Btime = datetime.timestamp(datetime.now())

# i = int(sys.argv[1])
# listeString = ''.join([' ' + str(random.randint(-i, i)) for x in range(i)])

def my_lister():
    file = open( r'C:\Users\LFRCAKE\Desktop\listes\liste2.txt', 'r' )
    my_list = file.read()
    file.close()
    y = []

    string = ''
    for x in my_list:

        if x == " ":
            y.append( string )
            string = ''
        else:
            string += x

    my_list = []
    for z in y:
        if z != '':
            my_list.append( z )
    return my_list

# listeBase = []
# entreeListe = ""
# for character in listeString:
#     if character == " ":
#         if entreeListe != "":
#             listeBase.append(int(entreeListe))
#         entreeListe = ""
#     else:
#         entreeListe += character

listeBase = my_lister()

sort = 1
while sort != 0:
    sort = 0
    nb = 0
    longListe = len(listeBase)
    while nb < longListe-1:
        if listeBase[nb] > listeBase[nb+1]:
            listeBase[nb], listeBase[nb+1] = listeBase[nb+1], listeBase[nb]
            sort += 1
        nb += 1
print(listeBase)

Etime = datetime.timestamp(datetime.now())
time = round(Etime - Btime, 2)
print("\ndurée du tri: "+str(time)+"s")

import sys
import time
from random import random
from datetime import datetime
Btime = datetime.timestamp(datetime.now())


def generate_list():
    i = int(sys.argv[1])
    a = 0
    my_list = []
    while a < i:
        my_list += [int(random.randint(-i, i))]
        a += 1
    print(my_list)
    return my_list


def sort_list_by_bubble_method(my_list):
    z = 0
    for _ in my_list:
        while z < len(my_list) - 1:
            if my_list[z] > my_list[z + 1]:
                my_list[z], my_list[z + 1] = my_list[z + 1], my_list[z]
            z += 1
        z = 0
    print(my_list)

def my_lister():
    file = open( r'C:\Users\LFRCAKE\Desktop\listes\liste2.txt', 'r' )
    my_list = file.read()
    file.close()
    y = []

    string = ''
    for x in my_list:

        if x == " ":
            y.append( string )
            string = ''
        else:
            string += x

    my_list = []
    for z in y:
        if z != '':
            my_list.append( z )
    return my_list

### ZONE D'EXECUTION ###
print("Méthode bubble sort :")
# tmps1 = time.time()
sort_list_by_bubble_method(my_lister())
Etime = datetime.timestamp(datetime.now())
time = round(Etime - Btime, 2)
print("\ndurée du tri: "+str(time)+"s")

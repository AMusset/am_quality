def insert_in_list(my_list, position, value):
    my_list += "a"
    z = len(my_list) - 1
    while z != position:
        my_list[z], my_list[z - 1] = my_list[z - 1], my_list[z]
        z -= 1
    my_list[position] = value
    return my_list


def sort_list_by_insertion_method(my_list):
    x = 1
    new_list = [my_list[0]]
    while x < len(my_list)-1:
        y = 0
        while my_list[x] > new_list[y]:
            y += 1
            if y == len(new_list):
                break
        insert_in_list(new_list, y, my_list[x])
        x += 1
    print(new_list)
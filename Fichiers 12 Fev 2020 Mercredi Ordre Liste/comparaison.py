#!/usr/bin/env python3
import sys
import random
from datetime import datetime

file = open(r'C:\Users\crunn\PycharmProjects\ExoSemainePython\Jour2\list.txt', 'r')
listString = file.read()
file.close()


def bubblesortJerem(stra):
    nbList = []
    nb = ""
    for c in stra:
        if c == ' ' and nb != "":
            nbList.append(int(nb))
            lenT = len(nbList)
            if lenT >= 2:
                i = 2
                notSort = True
                while notSort:
                    if nbList[lenT - (i - 1)] < nbList[lenT - i] and i <= lenT:
                        nbList[lenT - (i - 1)], nbList[lenT - i] = nbList[lenT - i], nbList[lenT - (i - 1)]
                        i += 1
                    else:
                        notSort = False
            nb = ""
        else:
            nb += c
    # print(nbList)


def bubblesortThomas(liste_string):
    listeBase = []
    entreeListe = ""
    for character in liste_string:
        if character == " ":
            if entreeListe != "":
                listeBase.append(int(entreeListe))
            entreeListe = ""
        else:
            entreeListe += character

    sort = 1
    while sort != 0:
        sort = 0
        nb = 0
        longListe = len(listeBase)
        while nb < longListe - 1:
            if listeBase[nb] > listeBase[nb + 1]:
                listeBase[nb], listeBase[nb + 1] = listeBase[nb + 1], listeBase[nb]
                sort += 1
            nb += 1
    # print(listeBase)


def bubblesortAlexis(my_list):
    y = []

    string = ''
    for x in my_list:

        if x == " ":
            y.append(string)
            string = ''
        else:
            string += x

    my_list = []
    for z in y:
        if z != '':
            my_list.append(z)

    z = 0
    for _ in my_list:
        while z < len(my_list) - 1:
            if my_list[z] > my_list[z + 1]:
                my_list[z], my_list[z + 1] = my_list[z + 1], my_list[z]
            z += 1
        z = 0
    # print(my_list)


def bubblesortKevin(my_list):
    y = []

    string = ''
    for x in my_list:

        if x == " ":
            y.append(string)
            string = ''
        else:
            string += x

    my_list = []
    for z in y:
        if z != '':
            my_list.append(z)

    # Bubble

    sort = 1
    while sort != 0:
        sort = 0
        i = 0
        while i < len(my_list) - 1:
            if int(my_list[i]) > int(my_list[i + 1]):
                my_list[i], my_list[i + 1] = my_list[i + 1], my_list[i]
                sort += 1
            i += 1
    # print(my_list)


def bubblesortPierreJean(my_list):
    y = []

    string = ''
    for x in my_list:

        if x == " ":
            y.append(string)
            string = ''
        else:
            string += x

    my_list = []
    for z in y:
        if z != '':
            my_list.append(z)

    n = len(my_list)
    for number in range(n):
        for element in range(0, n - number - 1):
            if int(my_list[element]) > int(my_list[element + 1]):
                exchange(my_list, element, element + 1)
    # print(my_list)


def bubblesortOpti(my_list):
    y = []

    string = ''
    for x in my_list:

        if x == " ":
            y.append(string)
            string = ''
        else:
            string += x

    my_list = []
    for z in y:
        if z != '':
            my_list.append(int(z))

    # Bubble
    sort = 1
    while sort != 0:
        sort = 0
        for x in range(0, len(my_list) - 1):
            if my_list[x] > my_list[x + 1]:
                my_list[x], my_list[x + 1] = my_list[x + 1], my_list[x]
                sort = 1
    # print(my_list)


# switch the positions of the elements
def exchange(string, firstposition, secondposition):
    string[firstposition], string[secondposition] = string[secondposition], string[firstposition]
    return string


Btime = datetime.timestamp(datetime.now())
print("\ndébut du tri d'Alexis")
bubblesortAlexis(listString)
timeAlexis = round(datetime.timestamp(datetime.now()) - Btime, 2)
print("\ndurée du tri: " + str(timeAlexis) + "s")

Btime = datetime.timestamp(datetime.now())
print("\ndébut du tri de Thomas")
bubblesortThomas(listString)
timeThomas = round(datetime.timestamp(datetime.now()) - Btime, 2)
print("\ndurée du tri: " + str(timeThomas) + "s")

Btime = datetime.timestamp(datetime.now())
print("\ndébut du tri de Kevin")
bubblesortKevin(listString)
timeKevin = round(datetime.timestamp(datetime.now()) - Btime, 2)
print("\ndurée du tri: " + str(timeKevin) + "s")

Btime = datetime.timestamp(datetime.now())
print("\ndébut du tri de Jerem")
bubblesortJerem(listString)
timeJerem = round(datetime.timestamp(datetime.now()) - Btime, 2)
print("\ndurée du tri: " + str(timeJerem) + "s")

Btime = datetime.timestamp(datetime.now())
print("\ndébut du tri de PierreJean")
bubblesortPierreJean(listString)
timePierreJean = round(datetime.timestamp(datetime.now()) - Btime, 2)
print("\ndurée du tri: " + str(timePierreJean) + "s")

Btime = datetime.timestamp(datetime.now())
print("\ndébut du tri Opti a partir du code de Kevin")
bubblesortOpti(listString)
timeOpti = round(datetime.timestamp(datetime.now()) - Btime, 2)
print("\ndurée du tri: " + str(timeOpti) + "s")

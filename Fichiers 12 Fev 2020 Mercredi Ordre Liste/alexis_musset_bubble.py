import sys
import time
import random


def generate_list():
    i = int(sys.argv[1])
    a = 0
    my_list = []
    while a < i:
        my_list += [int(random.randint(-i, i))]
        a += 1
    print(my_list)
    return my_list


def sort_list_by_bubble_method(my_list):
    z = 0
    for _ in my_list:
        while z < len(my_list) - 1:
            if my_list[z] > my_list[z + 1]:
                my_list[z], my_list[z + 1] = my_list[z + 1], my_list[z]
            z += 1
        z = 0
    print(my_list)


### ZONE D'EXECUTION ###
print("Méthode bubble sort :")
tmps1 = time.time()
sort_list_by_bubble_method(generate_list())
tmps2 = time.time() - tmps1
print("Temps d'execution = %f" % tmps2)

#!/usr/bin/env python3
import sys
import random
import time
from bubble_sort_base import *
from insertion_sort import *


def generate_list():
    i = int(sys.argv[1])
    a = 0
    my_list = []
    while a < i:
        my_list += [int(random.randint(-i, i))]
        a += 1
    print(my_list)
    return my_list


### ZONE D'EXECUTION ###
print("Méthode bubble sort :")
tmps1 = time.time()
sort_list_by_bubble_method(generate_list())
tmps2 = time.time() - tmps1
print("Temps d'execution = %f" % tmps2)

print("----------------------------------------------")

print("Méthode par insertion :")
tmps1 = time.time()
sort_list_by_insertion_method(generate_list())
tmps2 = time.time() - tmps1
print("Temps d'execution = %f" % tmps2)

print("----------------------------------------------")

print("Méthode par fusion : ...")
import sys
import random


def generate_list():
    i = int(sys.argv[1])
    a = 0
    my_list = []
    while a < i:
        my_list += [int(random.randint(-i, i))]
        a += 1
    print(my_list)
    return my_list


def choix_pivot(tab):
    return round(len(tab) / 2)


def partitionner(tab, premier, dernier, pivot):
    tab[pivot], tab[dernier] = tab[dernier], tab[pivot]
    p = premier
    for i in range(premier, dernier - 1):
        if tab[i] <= tab[dernier]:
            tab[p], tab[i] = tab[i], tab[p]
            p += 1
    tab[p], tab[dernier] = tab[dernier], tab[p]
    return p


def tri_rapide(tab, premier, dernier):
    if premier < dernier:
        pivot = round(len(tab) / 2)
        p = partitionner(tab, premier, dernier, pivot)
        tri_rapide(tab, premier, p - 1)
        tri_rapide(tab, p - 1, dernier)


to_sort = generate_list()
tri_rapide(to_sort, to_sort[0], to_sort[len(to_sort)-1])
print(to_sort)

import random
import re
from fuzzer_material import MATERIAL

re.compile(r'(<[^<> ]*>)')


def get_nonsense_word(length):
    first = random.choice(MATERIAL["<upper>"])
    body = random.choices(MATERIAL["<lower>"], k=length - 1)
    strBody = "".join(body)
    word = first + strBody
    return word


def get_email(min_len_radical, max_len_radical, min_nbr_separateur, max_nbr_separateur, len_presufixe):
    nb_sep = random.randint(min_nbr_separateur, max_nbr_separateur)
    seps = random.choices(MATERIAL["<separator>"], k=nb_sep)
    len_radical = random.randint(min_len_radical, max_len_radical)
    global_radical = get_nonsense_word(len_radical)
    nb_char = round(len(global_radical) / (nb_sep + 1))
    for i in range(1, nb_sep):
        global_radical = global_radical[:i * nb_char] + seps[i - 1] + global_radical[i * nb_char:]
    suf = random.choice(MATERIAL["<suffix>"])
    email = global_radical + "@" + get_nonsense_word(len_presufixe) + "." + suf
    return email


# def input_nb(a):
#    if a.isnumeric():
#        pass
#    else:
#        print("error")


answer = 0
while answer not in ["1", "2"]:
    answer = input(
        "=== FUZZER ===\nVous voulez\n1 - Un ou plusieurs mots\n2 - Une ou plusieures adresses mail\nNuméro :")

if answer == "1":
    nb_words = input("How many words ?")
    if nb_words != "1":
        min_letters = input("How many letters in your word(s) ? (set minimum)")
        max_letters = input("How many letters in your word(s) ? (set maximum)")
        print("Here you go :")
        for a in range(0, int(nb_words)):
            nb_letters = random.randint(int(min_letters), int(max_letters))
            print(get_nonsense_word(int(nb_letters)))
    elif nb_words == "1":
        nb_letters = input("How many letters ?")
        print("Here you go :", get_nonsense_word(int(nb_letters)))
    else:
        print("incorrect")

elif answer == "2":
    nb_mails = input("How many electronic adresses ?")
    this_min_len_rad = input("How many letters in the adress radical ? (set minimum)")
    this_max_len_rad = input("How many letters in the adress radical ? (set maximum)")
    this_min_nbr_sep = input("How many separators ? (set minimum)")
    this_max_nbr_sep = input("How many separators ? (set maximum)")
    this_len_presuf = input("How many letters in the suffix ? (set maximum)")
    for b in range(0, int(nb_mails)):
        print(get_email(int(this_min_len_rad), int(this_max_len_rad), int(this_min_nbr_sep), int(this_max_nbr_sep),
                        int(this_len_presuf)))

# print(get_nonsense_word(5))
# print(get_email(9, 21, 1, 3, 4))
